# This is my package laravel-https

[![Latest Version on Packagist](https://img.shields.io/packagist/v/kda/laravel-https.svg?style=flat-square)](https://packagist.org/packages/kda/laravel-https)
[![Total Downloads](https://img.shields.io/packagist/dt/kda/laravel-https.svg?style=flat-square)](https://packagist.org/packages/kda/laravel-https)

## Installation

You can install the package via composer:

```bash
composer require kda/laravel-https
```

## Usage
### enable globally
edit app/Http/Kernel.php and add the middleware at the end

```php
  protected $middleware = [
        // ... middlewares
        \KDA\LaravelHttps\Middleware\HttpsMiddleware::class
    ];
```


## Testing

```bash
composer test
```

## Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information on what has changed recently.

## Contributing

Please see [CONTRIBUTING](https://github.com/fdt2k/.github/blob/main/CONTRIBUTING.md) for details.

## Security Vulnerabilities

Please review [our security policy](../../security/policy) on how to report security vulnerabilities.

## Credits

- [Fabien Karsegard](https://github.com/fdt2k)
- [All Contributors](../../contributors)

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
